<?php

namespace App\Http\Controllers;

use Log,Auth,Queue;
use App\Exports\WithdrawalsExport;
use App\Exports\ContributionsExport;
use App\Exports\EmployeeContributionsExport;
use App\Exports\MyContributionExport;
use App\Exports\MyWithdrawalExport;

use App\Exports\AllContributionsExport;
use App\Exports\AllWithdrawalsExport;

use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;
use App\Contribution;
use App\Withdrawal;
use App\User;
use App\RollOver;
use App\Interest;
use PDF;
use Carbon\Carbon;


//Jobs Facade
use App\Jobs\ContributionsPDFExporter;
use App\Jobs\WithdrawalsPDFExporter;


class ExportController extends Controller
{

    //Excel sections

    public function withdrawal_export(Request $request){
        
        return (new WithdrawalsExport($request))->download('withdrawals-report'.now().'.xlsx');

    }

    public function contribution_export(Request $request){

        return (new ContributionsExport($request))->download('contributions-report'.now().'.xlsx');

    }


    public function employee_contribution_export_all(Request $request,$id){
        return (new EmployeeContributionsExport($id))->download('employee-contributions-report'.now().'.xlsx');

    }
    

    //PDF SECTIONS

    public function withdrawal_export_pdf(Request $request){

        if( !empty($request->keyword) ){
            
            $withdrawals = Withdrawal::with("approver","user")
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->where('remark','like','%' . $request->keyword . '%')
            ->whereOr('amount','like','%' . $request->keyword . '%')
            ->whereOr('approved_date','like','%' . $request->keyword . '%')
            ->whereOr('created_at','like','%' . $request->keyword . '%')
            ->paginate(20, ['*'], 'page', $request->page);


        }else if( $request->advance_search && ( !empty($request->region_id) || !empty($request->username) || !empty($request->start_date) || !empty($request->end_date) ) ){

            $users = User::where('region_id', $request->region_id)
            ->where('username', 'like', '%'.$request->username.'%')
            ->pluck("id");


            if( !empty($request->start_date) && !empty($request->end_date) ){


                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $request->start_date)
                ->where('created_at', '<=', $request->end_date)
                ->orderBy('created_at','DESC')
                ->paginate(20, ['*'], 'page', $request->page);
                

            }else if( !empty($request->start_date) ){

                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $request->start_date)
                ->orderBy('created_at','DESC')
                ->paginate(20, ['*'], 'page', $request->page);

            }else if( !empty($request->end_date) ){

                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->where('created_at', '<=', $request->end_date)
                ->orderBy('created_at','DESC')
                ->paginate(20, ['*'], 'page', $request->page);

            }else{

                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->orderBy('created_at','DESC')
                ->paginate(20, ['*'], 'page', $request->page);

            }

        }else{

            $withdrawals = Withdrawal::with("approver","user")
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->paginate(20, ['*'], 'page', $request->page);
            

        }

        $data = ['withdrawals' => $withdrawals];
        $pdf = PDF::loadView('pdf/withdrawals', $data);
  
        return $pdf->download('withdrawals-report'.now().'.pdf');

    }


    public function contribution_export_pdf(Request $request){

        if( !empty($request->keyword) ){

            $contributions = Contribution::with('creditor','contribution_type','user')
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->where('remark','like','%' . $request->keyword . '%')
            ->whereOr('user_id','=',$request->keyword)
            ->whereOr('amount','like','%' . $request->keyword . '%')
            ->whereOr('approved_date','like','%' . $request->keyword . '%')
            ->whereOr('created_at','like','%' . $request->keyword . '%')
            ->paginate(20, ['*'], 'page', $request->page);

        }else if( $request->advance_search && ( !empty($request->region_id) || !empty($request->username) || !empty($request->start_date) || !empty($request->end_date) ) ){

            
            $users = User::where('region_id', $request->region_id)
            ->where('username', 'like', '%'.$request->username.'%')
            ->pluck("id");

            if( !empty($request->start_date) && !empty($request->end_date) ){


                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $request->start_date)
                ->where('created_at', '<=', $request->end_date)
                ->orderBy('created_at','DESC')
                ->paginate(20, ['*'], 'page', $request->page);

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $request->start_date)
                ->where('created_at', '<=', $request->end_date)
                ->paginate(20, ['*'], 'page', $request->page);
                

            }else if( !empty($request->start_date) ){

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $request->start_date)
                ->paginate(20, ['*'], 'page', $request->page);

            }else if( !empty($request->end_date) ){

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->whereIn('user_id', $users)
                ->where('created_at', '<=', $request->end_date)
                ->paginate(20, ['*'], 'page', $request->page);

            }else{

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->whereIn('user_id', $users)
                ->paginate(20, ['*'], 'page', $request->page);

            }

        }else{
            
            $contributions = Contribution::with('creditor','contribution_type','user')
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->paginate(20, ['*'], 'page', $request->page);

        }

        
        $data = ['contributions' => $contributions];
        $pdf = PDF::loadView('pdf/contributions', $data);
        return $pdf->stream();  
        return $pdf->download('contributions-report'.now().'.pdf');

    }


    public function employee_contribution_export_all_pdf(Request $request,$id){


        $user = User::find($id);
        $roll_over = RollOver::where('user_id',$user->id)->orderBy('id','DESC')->first();
        
        if( !empty($roll_over) ){
            
            $contributions = Contribution::with('creditor','contribution_type','user')
            ->orderBy('created_at','ASC')
            ->whereDate('created_at', '>=' ,$roll_over->end_date)
            ->where('user_id',$id)->get();
        
        }else{
            
            $contributions = Contribution::with('creditor','contribution_type','user')
            ->orderBy('created_at','ASC')
            ->where('user_id',$id)->get();
            
        }

        $interest = Interest::orderBy('id','DESC')->first();
        
        if( !empty($roll_over) ){
            
            $withdrawal = Withdrawal::where('user_id',$user->id)
            ->whereDate('created_at', '>=' ,$roll_over->end_date)
            ->sum('amount');
        
        }else{
            
            $withdrawal = Withdrawal::where('user_id',$user->id)
            ->sum('amount');
            
        }
        

        $data = ['user'          => $user,
                 'roll_over'     => $roll_over,
                 'contributions' => $contributions,
                 'interest'      => $interest,
                 'withdrawal'    => $withdrawal];

        
        $pdf = PDF::loadView('pdf/employee_contributions_formatted', $data);

        //return $pdf->stream();
        return $pdf->download('employee-contributions-report'.now().'.pdf');

    }







    //Employee Exports
    public function my_contribution_export(Request $request){
        return (new MyContributionExport($request))->download('my-contributions-report'.now().'.xlsx');

    }

    public function my_contribution_pdf_export(){
        
        if( !empty($request->start_date) && !empty($request->end_date) ){
                
            $contributions = Contribution::with('creditor','contribution_type','user')
            ->where('user_id',Auth::user()->id)
            ->where('created_at', '>=', $request->start_date)
            ->where('created_at', '<=', $request->end_date)
            ->get();

        }else if( !empty($request->start_date) ){
            
            $contributions = Contribution::with('creditor','contribution_type','user')
            ->where('user_id',Auth::user()->id)
            ->where('created_at', '>=', $request->start_date)
            ->get();          

        }else if( !empty($request->end_date) ){
            
            $contributions = Contribution::with('creditor','contribution_type','user')
            ->where('user_id',Auth::user()->id)
            ->where('created_at', '<=', $request->end_date)
            ->get();

        }else{

            $contributions = Contribution::with('creditor','contribution_type','user')->where('user_id',Auth::user()->id)
            ->get();

        }

        $interest = Interest::orderBy('id','DESC')->first();
        $data = ['contributions' => $contributions,
                 'interest'      => $interest];

        $pdf = PDF::loadView('pdf/my_contributions', $data);
        return $pdf->stream();
        return $pdf->download('my-contributions-report'.now().'.pdf');
        
    }

    public function my_withdrawal_export(Request $request){
        return (new MyWithdrawalExport($request))->download('my-withdrawal-report'.now().'.xlsx');
        
    }

    public function my_withdrawal_pdf_export(){
        
        if( !empty($request->start_date) && !empty($request->end_date) ){
                
            $withdrawals = Withdrawal::with("approver")
            ->withTrashed()
            ->where('user_id',Auth::user()->id)
            ->where('created_at', '>=', $request->start_date)
            ->where('created_at', '<=', $request->end_date)
            ->get();

        }else if( !empty($request->start_date) ){
            
            $withdrawals = Withdrawal::with("approver")
            ->withTrashed()
            ->where('user_id',Auth::user()->id)
            ->where('created_at', '>=', $request->start_date)
            ->get();

        }else if( !empty($request->end_date) ){
            
            $withdrawals = Withdrawal::with("approver")
            ->withTrashed()
            ->where('user_id',Auth::user()->id)
            ->where('created_at', '<=', $request->end_date)
            ->get();

        }else{

            $withdrawals = Withdrawal::with("approver")->withTrashed()->where('user_id',Auth::user()->id)->get();

        }

        $data = ['withdrawals' => $withdrawals];
        $pdf = PDF::loadView('pdf/my_withdrawals', $data);
        
        return $pdf->download('my-withdrawals-report'.now().'.pdf');
        
    }









    //Export All To PDF
    public function export_all_contributions_pdf( Request $request ){

        // //Send Exporting to Job
        dispatch(new ContributionsPDFExporter() );
        $request->session()->flash('alert-success', 'Report Is Currently Been Exported. Check File Manager For Exported Reports');
        return redirect('contribution_report');

    }

    public function export_all_withdrawals_pdf( Request $request ){

        
        // //Send Exporting to Job
        dispatch(new WithdrawalsPDFExporter() );
        $request->session()->flash('alert-success', 'Report Is Currently Been Exported. Check File Manager For Exported Reports');
        return redirect('withdraw_report');

    }


    // Export All To Excel
    public function export_all_contributions_excel( Request $request ){

        // //Send Exporting to Job
        return (new AllContributionsExport())->download('all-contributions-report'.now().'.xlsx');

    }


    public function export_all_withdrawals_excel( Request $request ){

        
        return (new AllWithdrawalsExport())->download('all-withdrawals-report'.now().'.xlsx');
        
    }






}
