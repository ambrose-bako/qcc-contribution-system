<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DirectoryIterator;

class DownloadController extends Controller
{
    //

    public function contributions_listing( Request $request ){

        $files = true;
        $folder = "";

        if( empty($request->folder) ){

            $dir_path = public_path() . '/exports/pdf/contributions/'.Auth::id().'/';
            $dir = new DirectoryIterator($dir_path);
            $files = false;
        }else{

            $folder = $request->folder;

            if( in_array( $folder, [".",".."]) || $folder[0] == "." ){
                $request->session()->flash('alert-danger', 'Invalid Folder Selected');
                return back();

            }else{

                $dir_path = public_path() . '/exports/pdf/contributions/'.Auth::id().'/'.$folder.'';
                $dir = new DirectoryIterator($dir_path);
                $files = true;
                

            }

        }
                            
                            

                        
        return view('pages.contributions_exports_viewer')->with(["files"=>$files,"dir"=>$dir,"folder"=>$folder]);

    }


    public function withdrawals_listing( Request $request ){

        $files = true;
        $folder = "";

        if( empty($request->folder) ){

            $dir_path = public_path() . '/exports/pdf/withdrawals/'.Auth::id().'/';
            $dir = new DirectoryIterator($dir_path);
            $files = false;
        }else{

            $folder = $request->folder;

            if( in_array( $folder, [".",".."]) || $folder[0] == "." ){
                $request->session()->flash('alert-danger', 'Invalid Folder Selected');
                return back();

            }else{

                $dir_path = public_path() . '/exports/pdf/withdrawals/'.Auth::id().'/'.$folder.'';
                $dir = new DirectoryIterator($dir_path);
                $files = true;
                

            }

        }
                            
                            

                        
        return view('pages.withdrawals_exports_viewer')->with(["files"=>$files,"dir"=>$dir,"folder"=>$folder]);

    }





    public function download_contributions_pdf( Request $request ){

        $file = public_path() . '/exports/pdf/contributions/'.Auth::id().'/'.$request->folder.'/'.$request->file.'';
        $headers = array(
            'Content-Type: application/pdf',
          );
        return response()->download($file, $request->file, $headers);

    }


    public function download_withdrawals_pdf( Request $request ){

        $file = public_path() . '/exports/pdf/withdrawals/'.Auth::id().'/'.$request->folder.'/'.$request->file.'';
        $headers = array(
            'Content-Type: application/pdf',
          );
        return response()->download($file, $request->file, $headers);

    }




}
