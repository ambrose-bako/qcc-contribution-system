<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Region;

class RegionController extends Controller
{
    //

    function index(Request $request){

        $regions = Region::with('creator')->withTrashed()->get();

        return view('pages.regions')->with(["regions"=>$regions]);    

    }

    function add(Request $request){

        return view('pages.add_region');

    }


    function store(Request $request){

        $region = new Region();
        $region->name = $request->name;
        $region->creator_id = Auth::user()->id;
        $region->save();

        $request->session()->flash('alert-success', 'Region Added Successfully');
            return redirect('add_region');

    }


    function delete(Request $request,$id){

        $region = Region::find($id);
        $region->delete();

        $request->session()->flash('alert-success', 'Region Disabled Successfully');
            return redirect('regions');

    }


    function restore(Request $request,$id){

        $region = Region::withTrashed()->find($id);
        $region->restore();

        $request->session()->flash('alert-success', 'Region Enabled Successfully');
            return redirect('regions');

    }


    function edit(Request $request,$id){

        $region = Region::withTrashed()->find($id);
        return view('pages.edit_region')->with(["region"=>$region]);    

    }

    function save_edit(Request $request,$id){

        $region = Region::withTrashed()->find($id);
        $region->name = $request->name;
        $region->save();

        $request->session()->flash('alert-success', 'Region Updated Successfully');
            return redirect('regions');

    }


}
