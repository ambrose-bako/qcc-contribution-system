<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\User;
use App\UserDepartment;
use App\Department;
use App\ContributionType;
use App\Contribution;

class ContributionController extends Controller
{
    //

    function index(Request $request){

        $contributions = Contribution::with('creditor','contribution_type','user')->orderBy('id','desc')->paginate(20);
        return view('pages.contributions')->with(["contributions"=>$contributions]);
    }

    function edit(Request $request){

        $contribution = Contribution::find($request->id);
        $contributionTypes = ContributionType::get();
        return view('pages.edit_contribution')->with(["contribution"=>$contribution ,"contribution_types"=>$contributionTypes]);

    }


    function update(Request $request){

        
        $contribution = Contribution::find($request->id);

        $contributionType = ContributionType::find($request->contribution_type_id);

        if($contributionType->class == "0"){

            $user = User::find($contribution->user_id);
            $amount = ($contributionType->amount/100) * $user->salary;
            $contribution->amount = $amount;

        }else{
            $contribution->amount = $contributionType->amount;
        }

        $contribution->remark = $request->remark;
        $contribution->save();

        if( empty($request->place) ){
            $request->session()->flash('alert-success', 'User Contribution Updated Successfully');
            return redirect('contributions');
        }else{
            $request->session()->flash('alert-success', 'User Contribution Updated Successfully');
            return redirect('employee_contribution_report');
        }


    }

    function my_contributions(Request $request){
        
        if( !empty($request->advance_search) ){

            if( !empty($request->start_date) && !empty($request->end_date) ){
                
                $contributions = Contribution::with('creditor','contribution_type','user')
                ->where('user_id',Auth::user()->id)
                ->where('created_at', '>=', $request->start_date)
                ->where('created_at', '<=', $request->end_date)
                ->paginate(20);

            }else if( !empty($request->start_date) ){
                
                $contributions = Contribution::with('creditor','contribution_type','user')
                ->where('user_id',Auth::user()->id)
                ->where('created_at', '>=', $request->start_date)
                ->paginate(20);

            }else if( !empty($request->end_date) ){
                
                $contributions = Contribution::with('creditor','contribution_type','user')
                ->where('user_id',Auth::user()->id)
                ->where('created_at', '<=', $request->end_date)
                ->paginate(20);

            }else{

                $contributions = Contribution::with('creditor','contribution_type','user')->where('user_id',Auth::user()->id)->paginate(20);

            }

        }else{
            $contributions = Contribution::with('creditor','contribution_type','user')->where('user_id',Auth::user()->id)->paginate(20);
        }
         
        return view('pages.my_contributions')->with(["contributions"=>$contributions]);

    }


    function single_contribution(Request $request){
        
        $users = User::get();
        $contributionTypes = ContributionType::get();
        
        return view('pages.single_contribution')->with(["users"=>$users,"contribution_types"=>$contributionTypes]);
    }

    function process_single_contribution(Request $request){
        
        $contribution = new Contribution();
        $contribution->user_id = $request->user_id;
        $contribution->approver_id = Auth::user()->id;
        $contribution->contribution_type_id = $request->contribution_type_id;

        $contributionType = ContributionType::find($request->contribution_type_id);

        if($contributionType->class == "0"){

            $user = User::find($request->user_id);
            $amount = ($contributionType->amount/100) * $user->salary;
            $contribution->amount = $amount;

        }else{
            $contribution->amount = $contributionType->amount;
        }

        $contribution->remark = $request->remark;
        $contribution->save();

        $request->session()->flash('alert-success', 'User Contribution Added Successfully');
            return redirect('single_contribution');



    }


    function department_contribution(){

        $departments = Department::get();
        $contributionTypes = ContributionType::get();
        
        return view('pages.department_contribution')->with(["departments"=>$departments,"contribution_types"=>$contributionTypes]);

    }


    function process_department_contribution(Request $request){
        
        $contributionType = ContributionType::find($request->contribution_type_id);
        $userDepartments = UserDepartment::where('department_id',$request->department_id)->get();
        
        foreach($userDepartments as $userDepartment){
            
            $contribution = new Contribution();
            $contribution->user_id = $userDepartment->user_id;
            $contribution->approver_id = Auth::user()->id;

            $contribution->contribution_type_id = $contributionType->id;
            $contribution->amount = $contributionType->amount;
            $contribution->remark = $request->remark;
            $contribution->save();

        }

        

        $request->session()->flash('alert-success', 'Department Contribution Added Successfully');
            return redirect('department_contribution');



    }


}
