<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Region;
use App\Withdrawal;
use App\Contribution;
use App\User;
use DB;


class ReportController extends Controller
{

    function withdraw_report(Request $request){

        if( !empty($request->keyword) ){
            
            $withdrawals = Withdrawal::with("approver","user")
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->where('remark','like','%' . $request->keyword . '%')
            ->whereOr('amount','like','%' . $request->keyword . '%')
            ->whereOr('approved_date','like','%' . $request->keyword . '%')
            ->whereOr('created_at','like','%' . $request->keyword . '%')
            ->paginate(20);


        }else if( $request->advance_search && ( !empty($request->region_id) || !empty($request->username) || !empty($request->start_date) || !empty($request->end_date) ) ){

            $users = User::where('region_id', $request->region_id)
            ->where('username', 'like', '%'.$request->username.'%')
            ->pluck("id");


            if( !empty($request->start_date) && !empty($request->end_date) ){


                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $request->start_date)
                ->where('created_at', '<=', $request->end_date)
                ->orderBy('created_at','DESC')
                ->paginate(20);
                

            }else if( !empty($request->start_date) ){

                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $request->start_date)
                ->orderBy('created_at','DESC')
                ->paginate(20);

            }else if( !empty($request->end_date) ){

                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->where('created_at', '<=', $request->end_date)
                ->orderBy('created_at','DESC')
                ->paginate(20);

            }else{

                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->orderBy('created_at','DESC')
                ->paginate(20);

            }

        }else{

            $withdrawals = Withdrawal::with("approver","user")
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->paginate(20);


        }
        
        $regions = Region::get();
        return view('pages.with_drawals_report')->with(['withdrawals' => $withdrawals,'keyword' => $request->keyword, "regions"=> $regions ]);

    }



    function contribution_report( Request $request){

        if( !empty($request->keyword) ){
            
            $contributions = Contribution::with('creditor','contribution_type','user')
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->where('remark','like','%' . $request->keyword . '%')
            ->whereOr('amount','like','%' . $request->keyword . '%')
            ->whereOr('approved_date','like','%' . $request->keyword . '%')
            ->whereOr('created_at','like','%' . $request->keyword . '%')
            ->paginate(20);


        }else if( $request->advance_search && ( !empty($request->region_id) || !empty($request->username) || !empty($request->start_date) || !empty($request->end_date) ) ){

            $users = User::where('region_id', $request->region_id)
            ->where('username', 'like', '%'.$request->username.'%')
            ->pluck("id");

            if( !empty($request->start_date) && !empty($request->end_date) ){


                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $request->start_date)
                ->where('created_at', '<=', $request->end_date)
                ->orderBy('created_at','DESC')
                ->paginate(20);

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $request->start_date)
                ->where('created_at', '<=', $request->end_date)
                ->paginate(20);
                

            }else if( !empty($request->start_date) ){

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $request->start_date)
                ->paginate(20);

            }else if( !empty($request->end_date) ){

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->whereIn('user_id', $users)
                ->where('created_at', '<=', $request->end_date)
                ->paginate(20);

            }else{

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->whereIn('user_id', $users)
                ->paginate(20);

            }

        }else{

            $contributions = Contribution::with('creditor','contribution_type','user')
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->paginate(20);


        }
        
        $regions = Region::get();
        return view('pages.contributions_report')->with(["contributions"=>$contributions,'keyword' => $request->keyword, "regions"=> $regions ]);

    }


    function employee_contribution_report(Request $request){

        if( !empty($request->keyword) ){
            $users = User::where("first_name",'like','%'.$request->keyword.'%')
            ->orWhere("last_name",'like','%'.$request->keyword.'%')
            ->orWhere("email",'like','%'.$request->keyword.'%')
            ->orWhere("phone",'like','%'.$request->keyword.'%')
            ->orWhere("username",'like','%'.$request->keyword.'%')
            ->paginate(20);
            return view('pages.employee_contributions_report')->with(["users"=>$users]);
        }else{
            return view('pages.employee_contributions_report');
        }

    }


    function employee_contribution_report_process(Request $request){

        if( !empty($request->advance_search) ){

            if( !empty($request->start_date) && !empty($request->end_date) ){

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->where('user_id',$request->user_id)
                ->where('created_at', '>=', $request->start_date)
                ->where('created_at', '<=', $request->end_date)
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->paginate(20);

                return 1;

            }else if( !empty($request->start_date) ){

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->where('user_id',$request->user_id)
                ->where('created_at', '>=', $request->start_date)
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->paginate(20);

            }else if( !empty($request->end_date) ){
                
                $contributions = Contribution::with('creditor','contribution_type','user')
                ->where('user_id',$request->user_id)
                ->where('created_at', '<=', $request->end_date)
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->paginate(20);

            }else{

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->where('user_id',$request->user_id)
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->paginate(20);

            }


        }else{

            $contributions = Contribution::with('creditor','contribution_type','user')
            ->where('user_id',$request->user_id)
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->paginate(20);

        }
        

        return view('pages.employee_contributions_report')->with(["contributions"=>$contributions,'keyword' => $request->user_id]);

    }



}
