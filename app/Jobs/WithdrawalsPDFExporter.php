<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;



use App\Withdrawal;
use App\User;
use PDF;
use Log,File,Auth;
use Carbon\Carbon;


class WithdrawalsPDFExporter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;
    private $queryLimit = 1000;
    private $batch_id;
    private $loop;
    private $user_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $withdrawals;
    private $data_export;

    public function __construct( $batch_id = null, $loop = null, $user_id = null )
    {
        

        $firstJob = false;
        if( $user_id == null ){
            $this->user_id = Auth::id();
        }else{
            $this->user_id = $user_id;
        }
        

        if( $batch_id == null ){
            $batch_id = Carbon::now()->timestamp;
            $firstJob = true;
        }
        $this->batch_id = $batch_id;

        //put all export in one folder
        $path = public_path('exports/pdf/withdrawals/'.$this->user_id.'');
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $path = public_path('exports/pdf/withdrawals/'.$this->user_id.'/'.$batch_id.'');
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }


        if( $firstJob ){

            $this->withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->latest()
                ->limit( $this->queryLimit )
                ->get();

                $this->loop = 1;

        }else{

            $this->withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->latest()
                ->offset( $this->queryLimit * $loop )
                ->limit( $this->queryLimit )
                ->get();

                $this->loop = $loop + 1;

        }

        
        Log::info('Getting Data');
        Log::info( [ "Loop" => $this->loop ] );
        //Log::info($this->contributions);

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        //Log::info('Exporting Data');
        //Log::info('User Id  ===> '.$this->user_id.'');
        $data = ['withdrawals' => $this->withdrawals];
        $pdf = PDF::loadView('pdf/all/withdrawals', $data);
        $pdf->save(public_path().'/exports/pdf/withdrawals/'.$this->user_id.'/'.$this->batch_id.'/withdrawals-report-'.now().'.pdf');
        
        if( count( $this->withdrawals ) >= $this->queryLimit ){
            dispatch(new WithdrawalsPDFExporter( $this->batch_id, $this->loop, $this->user_id )  );
        }

    }


}
