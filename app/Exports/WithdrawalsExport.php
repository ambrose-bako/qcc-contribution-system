<?php

namespace App\Exports;


use App\Withdrawal;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class WithdrawalsExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;
    public function __construct($request)
    {
        $this->request = $request;
        $this->rows = 0;
    }

    

    public function collection()
    {

        if( !empty($this->request->keyword) ){
            
            $withdrawals = Withdrawal::with("approver","user")
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->where('remark','like','%' . $this->request->keyword . '%')
            ->whereOr('amount','like','%' . $this->request->keyword . '%')
            ->whereOr('approved_date','like','%' . $this->request->keyword . '%')
            ->whereOr('created_at','like','%' . $this->request->keyword . '%')
            ->paginate(20, ['*'], 'page', $this->request->page);


        }else if( $this->request->advance_search && ( !empty($this->request->region_id) || !empty($this->request->username) || !empty($this->request->start_date) || !empty($this->request->end_date) ) ){

            $users = User::where('region_id', $this->request->region_id)
            ->where('username', 'like', '%'.$this->request->username.'%')
            ->pluck("id");


            if( !empty($this->request->start_date) && !empty($this->request->end_date) ){


                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $this->request->start_date)
                ->where('created_at', '<=', $this->request->end_date)
                ->orderBy('created_at','DESC')
                ->paginate(20, ['*'], 'page', $this->request->page);
                

            }else if( !empty($this->request->start_date) ){

                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $this->request->start_date)
                ->orderBy('created_at','DESC')
                ->paginate(20, ['*'], 'page', $this->request->page);

            }else if( !empty($this->request->end_date) ){

                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->where('created_at', '<=', $this->request->end_date)
                ->orderBy('created_at','DESC')
                ->paginate(20, ['*'], 'page', $this->request->page);

            }else{

                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->orderBy('created_at','DESC')
                ->paginate(20, ['*'], 'page', $this->request->page);

            }

        }else{

            $withdrawals = Withdrawal::with("approver","user")
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->paginate(20, ['*'], 'page', $this->request->page);


        }


        return $withdrawals;

    }


    public function headings(): array
    {
        return [
            '#',
            'User',
            'Approved By',
            'Amount',
            'Remark',
            'Status',
            'Approved Date',
            'Created Date',
            'Updated Date',
        ];
    }


    public function map($withdrawal): array
    {
        
        $this->rows++;

        if(empty($withdrawal->approver)){

            return [
                $this->rows,
                $withdrawal->user->first_name.' '.$withdrawal->user->last_name,
                '',
                $withdrawal->amount,
                $withdrawal->remark,
                $withdrawal->status,
                $withdrawal->approved_date,
                $withdrawal->created_at,
                $withdrawal->updated_at,
            ];

        }else{

            return [
                $this->rows,
                $withdrawal->user->first_name.' '.$withdrawal->user->last_name,
                $withdrawal->approver->first_name.' '.$withdrawal->approver->last_name,
                $withdrawal->amount,
                $withdrawal->remark,
                $withdrawal->status,
                $withdrawal->approved_date,
                $withdrawal->created_at,
                $withdrawal->updated_at,
            ];

        }

        
    }

}
