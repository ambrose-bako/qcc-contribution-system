<?php

namespace App\Exports;

use Log;
use App\Contribution;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;


class AllContributionsExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;
    
    public function __construct()
    {
        $this->rows = 0;
    }

    

    public function collection()
    {

        $contributions = Contribution::with('creditor','contribution_type','user')
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->get();

        return $contributions;

    }


    public function headings(): array
    {
        return [
            '#',
            'User',
            'Amount',
            'Contribution Type',
            'Remark',
            'Credited By',
            'Credited On',
        ];
    }


    public function map($contribution): array
    {
        
        $this->rows++;

        return [
            $this->rows,
            $contribution->user->first_name.' '.$contribution->user->last_name,
            $contribution->amount,
            $contribution->contribution_type->name,
            $contribution->remark,
            $contribution->creditor->first_name.' '.$contribution->creditor->last_name,
            $contribution->created_at,
        ];

        
    }

}
