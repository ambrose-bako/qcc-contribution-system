<?php

namespace App\Exports;

use App\Contribution;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Auth,Log;

class MyContributionExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;
    public function __construct($request)
    {
        $this->request = $request;
        $this->rows = 0;
    }

    

    public function collection()
    {
        
        if( !empty($this->request->start_date) && !empty($this->request->end_date) ){
                
            $contributions = Contribution::with('creditor','contribution_type','user')
            ->where('user_id',Auth::user()->id)
            ->where('created_at', '>=', $this->request->start_date)
            ->where('created_at', '<=', $this->request->end_date)
            ->get();

        }else if( !empty($this->request->start_date) ){
            
            $contributions = Contribution::with('creditor','contribution_type','user')
            ->where('user_id',Auth::user()->id)
            ->where('created_at', '>=', $this->request->start_date)
            ->get();

        }else if( !empty($this->request->end_date) ){
            
            $contributions = Contribution::with('creditor','contribution_type','user')
            ->where('user_id',Auth::user()->id)
            ->where('created_at', '<=', $this->request->end_date)
            ->get();

        }else{

            $contributions = Contribution::with('creditor','contribution_type','user')->where('user_id',Auth::user()->id)->get();

        }

        return $contributions;

    }


    public function headings(): array
    {
        return [
            '#',
            'Fullname',
            'Amount',
            'Contribution Type',
            'Remark',
            'Credited By',
            'Credited On',
        ];
    }


    public function map($contribution): array
    {
        
        $this->rows++;

        return [
            $this->rows,
            $contribution->user->first_name.' '.$contribution->user->last_name,
            $contribution->amount,
            $contribution->contribution_type->name,
            $contribution->remark,
            $contribution->creditor->first_name.' '.$contribution->creditor->last_name,
            $contribution->created_at,
        ];

        
    }


}
