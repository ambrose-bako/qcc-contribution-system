<?php

namespace App\Exports;

use App\Withdrawal;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AllWithdrawalsExport implements FromCollection, WithHeadings, WithMapping
{


    use Exportable;
    public function __construct()
    {
        $this->rows = 0;
    }

    
    public function collection()
    {

        $withdrawals = Withdrawal::with("approver","user")
        ->withTrashed()
        ->orderBy('created_at','DESC')
        ->get();

        return $withdrawals;

    }


    public function headings(): array
    {
        return [
            '#',
            'User',
            'Approved By',
            'Amount',
            'Remark',
            'Status',
            'Approved Date',
            'Created Date',
            'Updated Date',
        ];
    }


    public function map($withdrawal): array
    {
        
        $this->rows++;

        if(empty($withdrawal->approver)){

            return [
                $this->rows,
                $withdrawal->user->first_name.' '.$withdrawal->user->last_name,
                '',
                $withdrawal->amount,
                $withdrawal->remark,
                $withdrawal->status,
                $withdrawal->approved_date,
                $withdrawal->created_at,
                $withdrawal->updated_at,
            ];

        }else{

            return [
                $this->rows,
                $withdrawal->user->first_name.' '.$withdrawal->user->last_name,
                $withdrawal->approver->first_name.' '.$withdrawal->approver->last_name,
                $withdrawal->amount,
                $withdrawal->remark,
                $withdrawal->status,
                $withdrawal->approved_date,
                $withdrawal->created_at,
                $withdrawal->updated_at,
            ];

        }

        
    }




}
