<?php

namespace App\Exports;

use Log;
use App\Contribution;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ContributionsExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;
    public function __construct($request)
    {
        $this->request = $request;
        $this->rows = 0;
    }

    

    public function collection()
    {

        if( !empty($this->request->keyword) ){

            $contributions = Contribution::with('creditor','contribution_type','user')
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->where('remark','like','%' . $this->request->keyword . '%')
            ->whereOr('user_id','=',$this->request->keyword)
            ->whereOr('amount','like','%' . $this->request->keyword . '%')
            ->whereOr('approved_date','like','%' . $this->request->keyword . '%')
            ->whereOr('created_at','like','%' . $this->request->keyword . '%')
            ->paginate(20, ['*'], 'page', $this->request->page);

        }else if( $this->request->advance_search && ( !empty($this->request->region_id) || !empty($this->request->username) || !empty($this->request->start_date) || !empty($this->request->end_date) ) ){

            
            $users = User::where('region_id', $this->request->region_id)
            ->where('username', 'like', '%'.$this->request->username.'%')
            ->pluck("id");

            if( !empty($this->request->start_date) && !empty($this->request->end_date) ){


                $withdrawals = Withdrawal::with("approver","user")
                ->withTrashed()
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $this->request->start_date)
                ->where('created_at', '<=', $this->request->end_date)
                ->orderBy('created_at','DESC')
                ->paginate(20, ['*'], 'page', $this->request->page);

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $this->request->start_date)
                ->where('created_at', '<=', $this->request->end_date)
                ->paginate(20, ['*'], 'page', $this->request->page);
                

            }else if( !empty($this->request->start_date) ){

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->whereIn('user_id', $users)
                ->where('created_at', '>=', $this->request->start_date)
                ->paginate(20, ['*'], 'page', $this->request->page);

            }else if( !empty($this->request->end_date) ){

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->whereIn('user_id', $users)
                ->where('created_at', '<=', $this->request->end_date)
                ->paginate(20, ['*'], 'page', $this->request->page);

            }else{

                $contributions = Contribution::with('creditor','contribution_type','user')
                ->withTrashed()
                ->orderBy('created_at','DESC')
                ->whereIn('user_id', $users)
                ->paginate(20, ['*'], 'page', $this->request->page);

            }

        }else{
            
            $contributions = Contribution::with('creditor','contribution_type','user')
            ->withTrashed()
            ->orderBy('created_at','DESC')
            ->paginate(20, ['*'], 'page', $this->request->page);

        }

        return $contributions;

    }


    public function headings(): array
    {
        return [
            '#',
            'User',
            'Amount',
            'Contribution Type',
            'Remark',
            'Credited By',
            'Credited On',
        ];
    }


    public function map($contribution): array
    {
        
        $this->rows++;

        return [
            $this->rows,
            $contribution->user->first_name.' '.$contribution->user->last_name,
            $contribution->amount,
            $contribution->contribution_type->name,
            $contribution->remark,
            $contribution->creditor->first_name.' '.$contribution->creditor->last_name,
            $contribution->created_at,
        ];

        
    }

}
