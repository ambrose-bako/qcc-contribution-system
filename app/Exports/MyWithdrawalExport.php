<?php

namespace App\Exports;

use App\Withdrawal;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Auth,Log;


class MyWithdrawalExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;
    public function __construct($request)
    {
        $this->request = $request;
        $this->rows = 0;
    }

    

    public function collection()
    {
        
        if( !empty($this->request->start_date) && !empty($this->request->end_date) ){
                
            $withdrawals = Withdrawal::with("approver")
            ->withTrashed()
            ->where('user_id',Auth::user()->id)
            ->where('created_at', '>=', $this->request->start_date)
            ->where('created_at', '<=', $this->request->end_date)
            ->get();

        }else if( !empty($this->request->start_date) ){
            
            $withdrawals = Withdrawal::with("approver")
            ->withTrashed()
            ->where('user_id',Auth::user()->id)
            ->where('created_at', '>=', $this->request->start_date)
            ->get();

        }else if( !empty($this->request->end_date) ){
            
            $withdrawals = Withdrawal::with("approver")
            ->withTrashed()
            ->where('user_id',Auth::user()->id)
            ->where('created_at', '<=', $this->request->end_date)
            ->get();

        }else{

            $withdrawals = Withdrawal::with("approver")->withTrashed()->where('user_id',Auth::user()->id)->get();

        }

        return $withdrawals;

    }


    public function headings(): array
    {
        return [
            '#',
            'Amount',
            'Remark',
            'Status',
            'Approved/Disapproved By',
            'Credited On',
        ];
    }


    public function map($withdrawal): array
    {
        
        $this->rows++;

        if( !empty($withdrawal->approver) ){

            return [
                $this->rows,
                $withdrawal->amount,
                $withdrawal->remark,
                $withdrawal->status,
                $withdrawal->approver->first_name.' '.$contribution->approver->last_name,
                $withdrawal->created_at,
            ];

        }else{
            
            return [
                $this->rows,
                $withdrawal->amount,
                $withdrawal->remark,
                $withdrawal->status,
                '',
                $withdrawal->created_at,
            ];

        }

        
    }


}
