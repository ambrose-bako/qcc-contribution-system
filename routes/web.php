<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Route::get('/upload','UserController@upload');


Route::get('/login', function () {
    return view('auth.login');
})->name("login");


Route::post('/login','AuthController@login')->name('process_login');
Route::get('/logout','AuthController@logout')->name('logout');




Route::group(array('middleware' => 'auth'), function(){

    Route::get('/dashboard','DashboardController@index');
    Route::get('/','DashboardController@index')->name('dashboard');


    Route::group(['middleware' => ['role:Admin|Super-Admin']], function () {

        Route::get('/departments','DepartmentController@index')->name('departments');
        Route::get('/add_department','DepartmentController@add')->name('add_department');
        Route::post('/add_department','DepartmentController@store')->name('add_department');
        Route::get('/delete_department/{id}','DepartmentController@delete')->name('delete_department');
        Route::get('/restore_department/{id}','DepartmentController@restore')->name('restore_department');
        Route::get('/edit_department/{id}','DepartmentController@edit')->name('edit_department');
        Route::post('/edit_department/{id}','DepartmentController@save_edit')->name('edit_department');

        Route::get('/regions','RegionController@index')->name('regions');
        Route::get('/add_region','RegionController@add')->name('add_region');
        Route::post('/add_region','RegionController@store')->name('add_region');
        Route::get('/delete_region/{id}','RegionController@delete')->name('delete_region');
        Route::get('/restore_region/{id}','RegionController@restore')->name('restore_region');
        Route::get('/edit_region/{id}','RegionController@edit')->name('edit_region');
        Route::post('/edit_region/{id}','RegionController@save_edit')->name('edit_region');
        

    
    
        Route::get('/users','UserController@index')->name('users');
        Route::get('/add_user','UserController@add')->name('add_user');
        Route::post('/add_user','UserController@store')->name('add_user');
        Route::get('/delete_user/{id}','UserController@delete')->name('delete_user');
        Route::get('/restore_user/{id}','UserController@restore')->name('restore_user');
        Route::get('/edit_user/{id}','UserController@edit')->name('edit_user');
        Route::post('/edit_user/{id}','UserController@save_edit')->name('edit_user');
    
    
        Route::get('/contribution_types','ContributionTypeController@index')->name('contribution_types');
        Route::get('/add_contribution_type','ContributionTypeController@add')->name('add_contribution_type');
        Route::post('/add_contribution_type','ContributionTypeController@store')->name('add_contribution_type');
        Route::get('/delete_contribution_type/{id}','ContributionTypeController@delete')->name('delete_contribution_type');
        Route::get('/restore_contribution_type/{id}','ContributionTypeController@restore')->name('restore_contribution_type');
        Route::get('/edit_contribution_type/{id}','ContributionTypeController@edit')->name('edit_contribution_type');
        Route::post('/edit_contribution_type/{id}','ContributionTypeController@save_edit')->name('edit_contribution_type');
        
        Route::get('/make_contribution',function () {
            return view('pages.initiate_contribution');
        })->name('make_contribution');
        Route::get('/single_contribution','ContributionController@single_contribution')->name('single_contribution');
        Route::post('/single_contribution','ContributionController@process_single_contribution')->name('single_contribution');
    
    
        Route::get('/department_contribution','ContributionController@department_contribution')->name('department_contribution');
        Route::post('/department_contribution','ContributionController@process_department_contribution')->name('department_contribution');
    
    
        Route::get('/contributions','ContributionController@index')->name('contributions');
        Route::get('/contribution/{id}','ContributionController@edit')->name('edit_contribution');
        Route::post('/contribution','ContributionController@update')->name('update_contribution');
        
        Route::get('/withdrawals','WithdrawalController@index')->name('withdrawals');
        Route::get('/disapprove_withdrawal/{id}','WithdrawalController@delete')->name('disapprove_withdrawal');
        Route::get('/approve_withdrawal/{id}','WithdrawalController@approve')->name('approve_withdrawal');
        
        
        
        Route::get('/report',function () {
            return view('pages.initiate_report');
        })->name('report');
    

        Route::get('/withdraw_report','ReportController@withdraw_report')->name('withdraw_report');
        Route::get('/contribution_report','ReportController@contribution_report')->name('contribution_report');
        Route::get('/employee_contribution_report','ReportController@employee_contribution_report')->name('employee_contribution_report');
        Route::post('/employee_contribution_report','ReportController@employee_contribution_report_process')->name('employee_contribution_report');

        

        //Importing Data
        Route::post('/process_user_import','ImportController@user_import')->name('process_user_import');
        Route::post('/process_contribution_import','ImportController@contribution_import')->name('process_contribution_import');
        
        Route::get('/import',function () {
            return view('pages.initiate_import');
        })->name('import');

        Route::get('/contribution_import',function () {
            return view('pages.contribution_import');
        })->name('contribution_import');

        Route::get('/user_import',function () {
            return view('pages.user_import');
        })->name('user_import');
          

        //GENERATING EXCEL EXPORT
        Route::post('/export_excel_withdrawals','ExportController@withdrawal_export')->name('export_excel_withdrawal');
        Route::post('/export_excel_contributions','ExportController@contribution_export')->name('export_excel_contribution');
        Route::get('/export_excel_employee_contributions/{id}','ExportController@employee_contribution_export_all')->name('export_excel_employee_contributions');

        //GENERATING PDF EXPORT
         Route::post('/export_pdf_withdrawals','ExportController@withdrawal_export_pdf')->name('export_pdf_withdrawal');
         Route::post('/export_pdf_contributions','ExportController@contribution_export_pdf')->name('export_pdf_contribution');
         Route::get('/export_pdf_employee_contributions/{id}','ExportController@employee_contribution_export_all_pdf')->name('export_pdf_employee_contributions');

         
         Route::get('/export_all_withdrawals_to_pdf','ExportController@export_all_withdrawals_pdf')->name('export_all_withdrawals_to_pdf');
         Route::get('/export_all_contributions_to_pdf','ExportController@export_all_contributions_pdf')->name('export_all_contributions_to_pdf');

         Route::get('/export_all_withdrawals_to_excel','ExportController@export_all_withdrawals_excel')->name('export_all_withdrawals_to_excel');
         Route::get('/export_all_contributions_to_excel','ExportController@export_all_contributions_excel')->name('export_all_contributions_to_excel');
        

         //Export Viewers
         Route::get('/exports',function () {
            return view('pages.initiate_exports_viewer');
        })->name('exports');

        Route::get('/exports_contributions',"DownloadController@contributions_listing")->name('contributions_exports');
        Route::get('/exports_withdrawals',"DownloadController@withdrawals_listing")->name('withdrawals_exports');

        //Download Exports
        Route::get('/download_contributions_exports',"DownloadController@download_contributions_pdf")->name('download_contributions_exports');
        Route::get('/download_withdrawals_exports',"DownloadController@download_withdrawals_pdf")->name('download_withdrawals_exports');
        


    });
    
    
    Route::get('/my_contributions','ContributionController@my_contributions')->name('my_contributions');
    Route::get('/my_withdrawals','WithdrawalController@my_withdrawals')->name('my_withdrawals');

    //EXPORTING REPORT
    Route::post('/my_contribution_excel_export','ExportController@my_contribution_export')->name('my_contribution_excel_export');
    Route::post('/my_contribution_pdf_export','ExportController@my_contribution_pdf_export')->name('my_contribution_pdf_export');
    
    Route::post('/my_withdrawal_excel_export','ExportController@my_withdrawal_export')->name('my_withdrawal_excel_export');
    Route::post('/my_withdrawal_pdf_export','ExportController@my_withdrawal_pdf_export')->name('my_withdrawal_pdf_export');
    


    Route::get('/make_withdrawal',function () {
        return view('pages.make_withdrawal');
    })->name('make_withdrawal');
    Route::post('/make_withdrawal','WithdrawalController@make_withdrawal')->name('make_withdrawal');

    Route::get('/profile','UserController@profile')->name('profile');

    Route::get('/edit_profile',function () {
        $user = Auth::user();
        return view('pages.edit_profile')->with(["user"=>$user]);
    })->name('edit_profile');
    
    Route::post('/update_profile','UserController@update_profile')->name('update_profile');

    Route::get('/change_password',function () {
        return view('pages.change_password');
    })->name('change_password');

    Route::post('/change_password','UserController@change_password')->name('change_password');
    
    
    //Route::get('/report','ReportController@index')->name('report');
    
    //RollOver Functions
    Route::get('/rollover','RollOverController@index')->name('roll_over');
    Route::post('/rollover','RollOverController@store')->name('process_roll_over');

    //Interest Functions
    Route::get('/interest','InterestController@index')->name('interest');
    Route::post('/interest','InterestController@store')->name('process_interest');
    
    
    

});
