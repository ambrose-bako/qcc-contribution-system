@extends("index")

@section("title", 'Contributions Report')

@section("content")


<br/><br/><br/><br/>
<div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div> <!-- end .flash-message -->

    @if( empty($users) && empty($contributions) )

    <div class="x_content">
            <br />

            <div class="col-md-3 col-lg-3 col-sm-6"></div>
            <div class="col-md-6 col-lg-6 col-sm-6">
            <p class="text-center h4">Please Search Employee For Contribution Reports</p><br/>
            <form class="form-inline text-center">
                <input type="text" class="form-control mb-2 mr-sm-2 col-10" id="inlineFormInputName2" placeholder="Name, Staff ID etc." name="keyword" required/>
                <button type="submit" class="btn btn-primary mb-2">Submit</button>
            </form>

        </div>
        <div class="col-md-3 col-lg-3 col-sm-6"></div>
    </div>
    
    @endif


    @if( !empty($users) )

        <div class="">

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2> <a href="{{ route('employee_contribution_report') }}" class="btn btn-sm btn-primary float-right"><i class="fa fa-arrow-left"></i> &nbsp;&nbsp;Search</a>  Employees Search Result</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <table id="example" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                            <th>ID</th>
                            <th>Fullname</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $user->fullname }} </td>
                                    <td>{{ $user->username }} </td>
                                    <td>{{ $user->email }} </td>
                                    <td>{{ $user->phone }} </td>
                                    <td>
                                        <form method="POST" action="{{ route('employee_contribution_report') }}">  
                                            @csrf                                          
                                            <input type="hidden" name="user_id" value="{{ $user->id }}" />
                                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> &nbsp;View </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                        </table>
                        <div class="pull-right">
                            {!! $users->links() !!}
                        </div>
                    </div>
                </div>
            </div>
            
        </div>  
    
    @endif


    @if(!empty($contributions))    
        <div class="">

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2> <a href="{{ URL::previous() }}" class="btn btn-sm btn-primary float-right"><i class="fa fa-arrow-left"></i> &nbsp;&nbsp;Back</a> Employee Contributions Report</h2>
                        <ul class="nav navbar-right panel_toolbox">
                                <li><a href="{{ route('export_pdf_employee_contributions',$keyword) }}" class="btn btn-danger btn-lg"><i class="fa fa-file"></i> PDF</a></li>
                                <li><a target="_blank" href="{{ route('export_excel_employee_contributions',$keyword) }}" class="btn btn-success btn-lg"><i class="fa fa-file"></i> EXCEL</a></li>
                            </ul>
                        <div class="clearfix"></div>
                    </div>

{{-- Advance Search Criteria --}}
@if( count($contributions) > 0 )

    <div class="row" style="margin:40px;">
            <div class="col-md-3"></div>
        <div class="col-md-6">
            <form method="POST">

                    @csrf
                    <input type="hidden" value="1" name="advance_search"/>
                    <input type="hidden" value="{{ $contributions[0]->user->id }}" name="user_id"/>
                    <div class="form-row">

                        <div class="form-group col-md-5">
                            <label for="dateRangePicker">Start Date</label>
                            <input type="date" class="form-control" id="dateRangePicker" placeholder="Start Date Picker" name="start_date" />
                        </div>
                        
                        <div class="form-group col-md-5">
                            <label for="dateRangePicker">End Date</label>
                            <input type="date" class="form-control" id="dateRangePicker" placeholder="End Date Picker" name="end_date" />
                        </div>

                    </div>
                    
                    <div class="form-group col-md-2">
                        <label for="dateRangePicker">&nbsp;&nbsp;&nbsp;</label>
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>

            </form>
        </div>
        <div class="col-md-3"></div>

    </div>

@endif

                    <div class="x_content">
                        <table id="example" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                            <th>ID</th>
                            <th>Fullname</th>
                            <th>Amount</th>
                            <th>Contribution Type</th>
                            <th>Remark</th>
                            <th>Credited By</th>
                            <th>Credited On</th>
                            <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach ($contributions as $contribution)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $contribution->user->fullname }} </td>
                                    <td>{{ $contribution->amount }} </td>
                                    <td>{{ $contribution->contribution_type->name }} </td>
                                    <td>{{ $contribution->remark }} </td>
                                    <td>{{ $contribution->creditor->fullname }} </td>
                                    <td>{{ $contribution->created_at }}</td>
                                    <td>
                                        <a class="btn btn-primary btn-xs" href="{{ route('edit_contribution',$contribution->id) }}?place=report">Edit <i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                        </table>
                        <div class="pull-right">
                            {!! $contributions->links() !!}
                        </div>
                    </div>
                </div>
            </div>

            
        </div>  
    @endif



  @endsection


@section("content")

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>


@endsection