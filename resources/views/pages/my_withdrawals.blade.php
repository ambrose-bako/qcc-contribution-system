@extends("index")

@section("title", 'My Withdrawals')

@section("content")


<br/><br/><br/><br/>
<div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div> <!-- end .flash-message -->

    
<div class="">
            
    <div class="clearfix"></div>

       <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>My Withdrawals</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a href="{{ route('make_withdrawal') }}" class="btn btn-primary btn-lg"><i class="fa fa-plus"></i> Request Withdrawal</a>
              </li>
              
              <li>
                  <form method="POST" action="{{ route('my_withdrawal_pdf_export') }}" target="_blank">
                      @csrf
                      @if( !empty( Request::get('advance_search') ) )

                          @if( !empty( Request::get('start_date') ) )
                              <input name="start_date" value="{{ Request::get('start_date') }}" type="hidden" />
                          @endif

                          @if( !empty( Request::get('end_date') ) )
                              <input name="end_date" value="{{ Request::get('end_date') }}" type="hidden" />
                          @endif

                      @endif

                      <button class="btn btn-danger btn-sm"> <i class="fa fa-file"></i> PDF</button>
                      
                  </form>
              </li>

              <li>
                  <form method="POST" action="{{ route('my_withdrawal_excel_export') }}" target="_blank">
                      @csrf
                      @if( !empty( Request::get('advance_search') ) )

                          @if( !empty( Request::get('start_date') ) )
                              <input name="start_date" value="{{ Request::get('start_date') }}" type="hidden" />
                          @endif

                          @if( !empty( Request::get('end_date') ) )
                              <input name="end_date" value="{{ Request::get('end_date') }}" type="hidden" />
                          @endif

                      @endif

                      <button class="btn btn-success btn-sm"> <i class="fa fa-file"></i> EXCEL</button>
                      
                  </form>
              </li>
              
            </ul>
            <div class="clearfix"></div>
          </div>

{{-- Advance Search Criteria --}}
<div class="row" style="margin:40px;">
      
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <form>

                @csrf
                <input type="hidden" value="1" name="advance_search"/>
                <div class="form-row">

                    <div class="form-group col-md-5">
                        <label for="dateRangePicker">Start Date</label>
                        <input type="date" class="form-control" id="dateRangePicker" placeholder="Start Date Picker" name="start_date" />
                    </div>
                    
                    <div class="form-group col-md-5">
                        <label for="dateRangePicker">End Date</label>
                        <input type="date" class="form-control" id="dateRangePicker" placeholder="End Date Picker" name="end_date" />
                    </div>

                </div>
                
                <div class="form-group col-md-2">
                    <label for="dateRangePicker">&nbsp;&nbsp;&nbsp;</label>
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>

        </form>
    </div>
    <div class="col-md-3"></div>

</div>

          <div class="x_content">
            <table id="example" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Amount</th>
                  <th>Remark</th>
                  <th>Status</th>
                  <th>Approved/Disapproved By</th>
                  <th>Credited On</th>
                </tr>
              </thead>

              <tbody>

                @foreach ($withdrawals as $withdrawal)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ env('APP_CURRENCY').' '.$withdrawal->amount }} </td>
                        <td>{{ $withdrawal->remark }} </td>
                        <td>
                            @if(empty($withdrawal->deleted_at))
                              {{ $withdrawal->status }} 
                            @else
                              <i class="btn btn-xs btn-danger">Disapproved</i>
                            @endif
                        </td>
                        <td>
                            @if(!empty($withdrawal->approver))
                                {{ $withdrawal->approver->fullname }}
                            @else
                                
                            @endif
                        </td>
                        <td>{{ $withdrawal->created_at }}</td>
                    </tr>
                @endforeach
                
              </tbody>
            </table>
            <div class="pull-right">
                {!! $withdrawals->links() !!}
            </div>
          </div>
        </div>
      </div>

      
  </div>


  
@endsection


@section("content")

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>


@endsection