@extends("index")

@section("title", 'Withdrawals Exports')

@section("content")


@if  (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Super-Admin'))

<div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div> <!-- end .flash-message -->

    
<div class="">
    <br/><br/><br/><br/><br/>
    <div class="row top_tiles">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 h4">
            Files
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
    </div>
    <br/><br/>
    <div class="row">
        
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 well">
                
                    
                @if( !$files )

                    <table class="table table-hover">
                        <caption>List of Exported Folders</caption>
                        @foreach ( $dir as $fileinfo )
                            <tr><td> <a href="?folder={{ $fileinfo }}">  {!! $fileinfo !!} </a></td></tr>
                        @endforeach
                    </table>

                @else

                    <div>
                        <a href="{{ route('withdrawals_exports') }}" class="btn btn-sm btn-default pull-right"> Back <i class="fa fa-back"></i> </a>
                    </div>

                    <table class="table table-hover">
                        <caption>List of Exported Folders</caption>
                        @foreach ( $dir as $fileinfo )

                            @if( $fileinfo->getExtension () === "pdf" )
                                <tr><td> <a href="{{ route('download_withdrawals_exports') }}?folder={{ $folder }}&file={{ $fileinfo }}" target="_blank"> {{ $fileinfo }} </a></td></tr>
                            @endif

                        @endforeach
                    </table>
                    
                @endif
                    
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
        
    </div>
  </div>

@else

<div class="row"> 
    
</div>

@endif



  
@endsection