@extends("index")

@section("title", 'Contributions')

@section("content")


<br/><br/><br/><br/>
<div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div> <!-- end .flash-message -->

    
<div class="">
            
    <div class="clearfix"></div>

       <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Contributions</h2>
            <ul class="nav navbar-right panel_toolbox">

                <li>
                    <form method="POST" action="{{ route('my_contribution_pdf_export') }}" target="_blank">
                        @csrf
                        @if( !empty( Request::get('advance_search') ) )

                            @if( !empty( Request::get('start_date') ) )
                                <input name="start_date" value="{{ Request::get('start_date') }}" type="hidden" />
                            @endif

                            @if( !empty( Request::get('end_date') ) )
                                <input name="end_date" value="{{ Request::get('end_date') }}" type="hidden" />
                            @endif

                        @endif

                        <button class="btn btn-danger btn-sm"> <i class="fa fa-file"></i> PDF</button>
                        
                    </form>
                </li>

                <li>
                    <form method="POST" action="{{ route('my_contribution_excel_export') }}" target="_blank">
                        @csrf
                        @if( !empty( Request::get('advance_search') ) )

                            @if( !empty( Request::get('start_date') ) )
                                <input name="start_date" value="{{ Request::get('start_date') }}" type="hidden" />
                            @endif

                            @if( !empty( Request::get('end_date') ) )
                                <input name="end_date" value="{{ Request::get('end_date') }}" type="hidden" />
                            @endif

                        @endif

                        <button class="btn btn-success btn-sm"> <i class="fa fa-file"></i> EXCEL</button>
                        
                    </form>
                </li>

            </ul>
            <div class="clearfix"></div>
          </div>

{{-- Advance Search Criteria --}}
<div class="row" style="margin:40px;">
      
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <form>

                @csrf
                <input type="hidden" value="1" name="advance_search"/>
                <div class="form-row">

                    <div class="form-group col-md-5">
                        <label for="dateRangePicker">Start Date</label>
                        <input type="date" class="form-control" id="dateRangePicker" placeholder="Start Date Picker" name="start_date" />
                    </div>
                    
                    <div class="form-group col-md-5">
                        <label for="dateRangePicker">End Date</label>
                        <input type="date" class="form-control" id="dateRangePicker" placeholder="End Date Picker" name="end_date" />
                    </div>

                </div>
                
                <div class="form-group col-md-2">
                    <label for="dateRangePicker">&nbsp;&nbsp;&nbsp;</label>
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>

        </form>
    </div>
    <div class="col-md-3"></div>

</div>


          <div class="x_content">
            <table id="example" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Fullname</th>
                  <th>Amount</th>
                  <th>Contribution Type</th>
                  <th>Remark</th>
                  <th>Credited By</th>
                  <th>Credited On</th>
                  {{-- <th>Action</th> --}}
                </tr>
              </thead>

              <tbody>

                @foreach ($contributions as $contribution)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $contribution->user->fullname }} </td>
                        <td>{{ $contribution->amount }} </td>
                        <td>{{ $contribution->contribution_type->name }} </td>
                        <td>{{ $contribution->remark }} </td>
                        <td>{{ $contribution->creditor->fullname }} </td>
                        <td>{{ $contribution->created_at }}</td>
                        {{-- <td>
                            @if(Auth::user()->id != $user->id)
                                <a class="btn btn-primary btn-xs" href="{{ route('edit_user',$user->id) }}"><i class="fa fa-edit"></i></a>

                                @if( empty($user->deleted_at) )
                                    <a class="btn btn-danger btn-xs" href="{{ route('delete_user',$user->id) }}"><i class="fa fa-trash"></i></a>
                                @else
                                    <a class="btn btn-success btn-xs" href="{{ route('restore_user',$user->id) }}"><i class="fa fa-check"></i></a>
                                @endif
                            @else

                                <button class="btn btn-primary btn-xs">Current User</button>

                            @endif
                            
                            
                        </td> --}}
                    </tr>
                @endforeach

                @if( count($contributions) < 1 )
                    <tr> <td colspan="7" class="text-center">No Contribution Found</td> </tr>
                @endif
                
              </tbody>
            </table>
            <div class="pull-right">
                {!! $contributions->links() !!}
            </div>
          </div>
        </div>
      </div>

      
  </div>


  
@endsection


@section("content")

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>


@endsection