@extends("index")

@section("title", 'Contributions Report')

@section("content")


<br/><br/><br/><br/>
<div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div> <!-- end .flash-message -->

    
<div class="">
            
    <div class="clearfix"></div>

       <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Contributions Report</h2>
            <ul class="nav navbar-right panel_toolbox">

                <li>
                    <form action="{{ route('export_all_contributions_to_pdf') }}" >
                        <button class="btn btn-danger btn-sm" type="submit"> <i class="fa fa-file"></i> Export All Data To PDF</button>
                    </form>
                </li>

                <li>
                    <form action="{{ route('export_all_contributions_to_excel') }}" >
                        <button class="btn btn-success btn-sm" type="submit"> <i class="fa fa-file"></i> Export All Data To Excel</button>
                    </form>
                </li>

                <li> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </li>

                <li>
                    <form method="POST" action="{{ route('export_pdf_contribution') }}" target="_blank">
                        
                        @csrf
                        @if( !empty( $keyword ) )
                            <input name="keyword" value="{{ $keyword }}" type="hidden" />
                        @endif

                        @if( !empty( Request::get('advance_search') ) )

                            <input name="advance_search" value="{{ Request::get('advance_search') }}" type="hidden" />

                            @if( !empty( Request::get('start_date') ) )
                                <input name="start_date" value="{{ Request::get('start_date') }}" type="hidden" />
                            @endif

                            @if( !empty( Request::get('end_date') ) )
                                <input name="end_date" value="{{ Request::get('end_date') }}" type="hidden" />
                            @endif

                            @if( !empty( Request::get('username') ) )
                                <input name="username" value="{{ Request::get('username') }}" type="hidden" />
                            @endif

                            @if( !empty( Request::get('region_id') ) )
                                <input name="region_id" value="{{ Request::get('region_id') }}" type="hidden" />
                            @endif

                        @endif


                        <input name="page" value="{{ $contributions->currentPage() }}" type="hidden" />

                        <button class="btn btn-danger btn-sm"> <i class="fa fa-file"></i> PDF</button>
                        
                    </form>
                </li>

                <li>
                    <form method="POST" action="{{ route('export_excel_contribution') }}" target="_blank">
                        @csrf
                        @if( !empty( $keyword ) )
                            <input name="keyword" value="{{ $keyword }}" type="hidden" />
                        @endif

                        @if( !empty( Request::get('advance_search') ) )

                            <input name="advance_search" value="{{ Request::get('advance_search') }}" type="hidden" />
                            @if( !empty( Request::get('start_date') ) )
                                <input name="start_date" value="{{ Request::get('start_date') }}" type="hidden" />
                            @endif

                            @if( !empty( Request::get('end_date') ) )
                                <input name="end_date" value="{{ Request::get('end_date') }}" type="hidden" />
                            @endif

                            @if( !empty( Request::get('username') ) )
                                <input name="username" value="{{ Request::get('username') }}" type="hidden" />
                            @endif

                            @if( !empty( Request::get('region_id') ) )
                                <input name="region_id" value="{{ Request::get('region_id') }}" type="hidden" />
                            @endif

                        @endif

                        <input name="page" value="{{ $contributions->currentPage() }}" type="hidden" />

                        <button class="btn btn-success btn-sm"> <i class="fa fa-file"></i> EXCEL</button>
                        
                    </form>
                </li>
                
            </ul>
            <div class="clearfix"></div>
          </div>


          <br/><br/>
        <div>
            <div class="col-md-4 col-lg-4 col-sm-6"></div>
            <div class="col-md-4 col-lg-4 col-sm-6">
                <form action="" method="GET">
                    @csrf
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search with e.g Fullname, Date, Remark" required name="keyword"/>
                        <span class="input-group-btn">
                            <input type="submit" class="form-control btn btn-primary" value="Search" />
                        </span>
                    </div><!-- /input-group -->
                </form>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6"></div>
        </div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#advanceSearchModal"> Advance Search </button>
        <br/><br/><br/><br/>


          <div class="x_content">
            <table id="example" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Fullname</th>
                  <th>Amount</th>
                  <th>Contribution Type</th>
                  <th>Remark</th>
                  <th>Credited By</th>
                  <th>Credited On</th>
                </tr>
              </thead>

              <tbody>

                @foreach ($contributions as $contribution)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $contribution->user->fullname }} </td>
                        <td>{{ $contribution->amount }} </td>
                        <td>{{ $contribution->contribution_type->name }} </td>
                        <td>{{ $contribution->remark }} </td>
                        <td>{{ $contribution->creditor->fullname }} </td>
                        <td>{{ $contribution->created_at }}</td>
                    </tr>
                @endforeach
                
              </tbody>
            </table>
            <div class="pull-right">
                {!! $contributions->links() !!}
            </div>
          </div>
        </div>
      </div>

      
  </div>



<!-- Modal -->
<div class="modal fade" id="advanceSearchModal" tabindex="-1" role="dialog" aria-labelledby="advanceSearchModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <div class="row">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                {{-- Advance Search Criteria --}}
                <form method="get" action="?advance_search=1">

                        @csrf
                        <input type="hidden" value="1" name="advance_search"/>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label for="staffID">Staff ID</label>
                            <input type="username" class="form-control" id="staffID" placeholder="Staff ID" name="username" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputState">Region</label>
                                <select id="inputState" class="form-control" name="region_id">
                                
                                    @foreach ($regions as $region)
                                        <option value="{{ $region->id }}"> {{ $region->name }} </option>
                                    @endforeach

                                </select>
                            </div>
                            
                        </div>
                        <div class="form-row">

                            <div class="form-group col-md-6">
                                <label for="dateRangePicker">Start Date</label>
                                <input type="date" class="form-control" id="dateRangePicker" placeholder="Start Date Picker" name="start_date" />
                            </div>
                            
                            <div class="form-group col-md-6">
                                <label for="dateRangePicker">End Date</label>
                                <input type="date" class="form-control" id="dateRangePicker" placeholder="End Date Picker" name="end_date" />
                            </div>

                        </div>
                        
                        <div class="form-row">
                            <button type="submit" class="btn btn-primary pull-right">Search</button>
                        </div>

                </form>

            </div>
        </div>
    </div>
</div>


@endsection


@section("content")

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>


@endsection