<!DOCTYPE html>
<html>
<head>
    <title></title>
    
    <style>

            .p2{
                padding: 5px;
            }
    
            table {
                border-collapse: collapse;
            }
    
            table, th, td {
                border: 1px solid black;
            }
    
            table {
                width: 100%;
            }
    
            th {
                height: 20px;
            }
    
        </style>

</head>
<body>

    <p style="text-align:center;padding:10px;"><h3> Contributions Report </h3></p>
<table>
        <thead>
            <tr>
                <th class="p2" >ID</th>
                <th class="p2" >Fullname</th>
                <th class="p2" >Amount</th>
                <th class="p2" >Contribution Type</th>
                <th class="p2" >Remark</th>
                <th class="p2" >Credited By</th>
                <th class="p2" >Credited On</th>
            </tr>
        </thead>

        <tbody>

            {{-- @foreach ($contributions as $contributionArray) --}}
            @foreach ($contributions as $contribution)

                {{-- @foreach ($contributionArray as $contribution) --}}
                    <tr>
                        <td class="p2" >{{ $loop->iteration }}</td>
                        <td class="p2" >{{ $contribution->user->fullname }} </td>
                        <td class="p2" >{{ $contribution->amount }} </td>
                        <td class="p2" >{{ $contribution->contribution_type->name }} </td>
                        <td class="p2" >{{ $contribution->remark }} </td>
                        <td class="p2" >{{ $contribution->creditor->fullname }} </td>
                        <td class="p2" >{{ $contribution->created_at }}</td>
                    </tr>
                {{-- @endforeach --}}

            @endforeach
            
        </tbody>
</table>

</body>
</html>