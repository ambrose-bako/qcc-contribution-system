<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>

            .p2{
                padding: 5px;
            }
    
            table {
                border-collapse: collapse;
            }
    
            table, th, td {
                border: 1px solid black;
            }
    
            table {
                width: 100%;
            }
    
            th {
                height: 20px;
            }
    
        </style>
</head>
<body>

    <p style="text-align:center;padding:10px;"><h3> Withdrawals Report </h3></p>
<table>
        <thead>
            <tr>
                <th class="p2">ID</th>
                <th class="p2">Fullname</th>
                <th class="p2">Amount</th>
                <th class="p2">Remark</th>
                <th class="p2">Credited On</th>
                <th class="p2">Status</th>
            </tr>
        </thead>

        <tbody>

                @foreach ($withdrawals as $withdrawal)
                <tr>
                    <td class="p2">{{ $loop->iteration }}</td>
                    <td class="p2">{{ $withdrawal->user->fullname }} </td>
                    <td class="p2">{{ $withdrawal->amount }} </td>
                    <td class="p2">{{ $withdrawal->remark }} </td>
                    <td class="p2">{{ $withdrawal->created_at }}</td>
                    <td class="p2">
                        @if(!empty($withdrawal->deleted_at))
                            <i class="btn btn-danger btn-sm">Disapproved</i>
                        @elseif(!empty($withdrawal->approved_date))
                            <i class="btn btn-success btn-sm">Approved</i>
                        @else
                            <i class="btn btn-warning btn-sm">Pending</i>
                        @endif
                    </td>
                </tr>
            @endforeach
            
        </tbody>
</table>

</body>
</html>