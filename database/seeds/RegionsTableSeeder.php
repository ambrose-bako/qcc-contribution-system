<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('regions')->delete();
        
        \DB::table('regions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Greater Accra',
                'creator_id' => 1,
                'created_at' => '2019-09-10 14:01:52',
                'updated_at' => '2019-09-10 14:02:34',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Ashanti',
                'creator_id' => 1,
                'created_at' => '2019-09-10 14:04:13',
                'updated_at' => '2019-09-10 14:04:26',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}