<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('picture_url')->nullable()->nullable();
            $table->string('email')->nullable()->unique();
            $table->string('phone')->unique()->nullable();;
            $table->string('username')->unique();
            $table->string('address')->nullable();
            $table->double('salary',10,2)->default(0);
            $table->double('opening_balance',10,2)->default(0);
            $table->double('withdrawals',10,2)->default(0);
            $table->string('job_description')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->unsignedBigInteger('region_id')->nullable();
            $table->enum('status', ['active', 'retired', 'disabled', 'suspended'])->nullable()->default('active');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            //$table->foreign('region_id')->references('id')->on('regions');
        });


        Schema::table('users',function($table){
            $table->foreign('region_id')->references('id')->on('regions');
        });

        Schema::table('regions',function($table){
            $table->foreign('creator_id')->references('id')->on('users');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
